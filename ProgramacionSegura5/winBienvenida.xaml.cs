﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ProgramacionSegura5
{
    /// <summary>
    /// Lógica de interacción para winBienvenida.xaml
    /// </summary>
    public partial class winBienvenida : Window
    {
        public winBienvenida()
        {
            InitializeComponent();
            txtRol.Text = "Roles: ";
            txtNombre.Text = "Hola, " + Sesion.usuarioSesion;
            UsuarioDAL dal = new UsuarioDAL();
            foreach (var rol in dal.ListaRoles())
            {
                txtRol.Text += "\t";
                txtRol.Text += rol;
                txtRol.Text += "\n";
            }
        }
    }
}
