﻿using System;
using System.Text.RegularExpressions;

namespace ProgramacionSegura5
{
    public class Validates
    {
      
        public static bool Contrasenia(string cad)
        {
            byte letras = 0;
            byte numeros = 0;

            if (cad.Length < 8)
            {
                return false;
            }

            for (int i = 0; 
                i < cad.Length; i++)
            {
                if (!char.IsLetter(cad[i]))
                {
                    if (!char.IsNumber(cad[i]))
                    {
                        if ((cad[i] == '%' || cad[i] == ' '))
                            return false;
                    }
                    else
                        numeros++;
                }
                else
                    letras++;
            }
            if (letras > 0 && numeros > 0 )
                return true;
            else
                return false;
        }
        public static bool Usuario(string cad)
        {

            for (int i = 0;
                i < cad.Length; i++)
            {
                if ((cad[i] == '%'))
                    return false;
            }
            if (cad.Length<8)
            {
                return false;
            }
            return true;
        }
    }
}
