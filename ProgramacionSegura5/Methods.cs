﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace ProgramacionSegura5
{
    public sealed class Methods
    {
        //private static string connectionString = ConfigurationManager.ConnectionStrings["LibreriaSanAntonio.Properties.Settings.BDLibreriaSanAntonioConnectionString"].ConnectionString;
        private static string connectionString = @"Data Source=LOCALHOST\SQLEXPRESS;Initial Catalog=ProgramacionSegura;Integrated Security=True";
        //private static string connectionString = @"Data Source=DESKTOP-KAPVOSK\SQLEXPRESS;Initial Catalog=BDLibreriaSanAntonio;Persist Security Info=True;User ID=sa;Password=dotaallstars";
        /// <summary>
        /// Retorna un comando sql relacionado a la conexion
        /// </summary>
        /// <param name="query">consulta para ejecutar el comando</param>
        /// <returns></returns>
        public static SqlCommand CreateBasicCommand(string query)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query)
            {
                Connection = connection
            };
            return cmd;
        }
        /// <summary>
        /// recibe un sql command y lo ejecuta con ExecuteNonQuery();  
        /// </summary>
        /// <param name="cmd"> comando relacionado con una conexion</param>
        public static void ExecuteBasicCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// recibe un comando lo ejecuta y retorna el resultado en un datatable
        /// </summary>
        /// <param name="cmd">sql command con consulta select</param>
        /// <returns>data table con resultado de un select</returns>
        public static DataTable ExcecuteDataTableCommand(SqlCommand cmd)
        {
            DataTable res = new DataTable();

            try
            {
                cmd.Connection.Open();
                SqlDataAdapter data = new SqlDataAdapter(cmd);//la conexion ya esta abierta
                data.Fill(res);

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return res;
        }
        /// <summary>
        /// devuelve un data reader
        /// </summary>
        /// <param name="cmd"></param>
        /// <returns></returns>
        public static SqlDataReader ExcecuteDataReaderCommand(SqlCommand cmd)
        {
            SqlDataReader res = null;
            try
            {
                cmd.Connection.Open();
                res = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public static DataTable ExecuteDataTableCommand(SqlCommand cmd)
        {
            DataTable res = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                sqlDataAdapter.Fill(res);
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
       
    }
}
