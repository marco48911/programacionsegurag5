﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
namespace ProgramacionSegura5
{
    public class UsuarioDAL
    {
        public UsuarioDAL()
        {

        }
        public DataTable Login(string nombreUsuario, string password)
        {
            DataTable res = new DataTable();
            SqlDataReader dr = null;
            string query = "SELECT idUsuario,usuario,nombre  FROM Usuario WHERE  usuario=@usuario AND password=HASHBYTES('md5',@password)";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@usuario", nombreUsuario);
                cmd.Parameters.AddWithValue("@password", password).SqlDbType = SqlDbType.VarChar;

                res = Methods.ExecuteDataTableCommand(cmd);

                dr = Methods.ExcecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    Sesion.idSesion = int.Parse(dr[0].ToString());
                    Sesion.usuarioSesion = dr[1].ToString();
                    Sesion.rolSesion = dr[2].ToString();
                }

            }
            catch (Exception)
            {
            }
            return res;
        }
        public List<string> ListaRoles()
        {
            List<string> roles = new List<string>();
            SqlDataReader dr = null;
            string query = @"SELECT tipo
                            FROM Rol R
                            INNER JOIN UsuarioRol UR ON UR.idRol = r.idRol
                            WHERE UR.idUsuario = @idUsuario";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idUsuario", Sesion.idSesion);

                dr = Methods.ExcecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    roles.Add(dr[0].ToString());
                }
            }
            catch (Exception)
            {
            }
            return roles;
        }
    }
}
