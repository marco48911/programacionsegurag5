﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ProgramacionSegura5
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (Validates.Usuario(txtUser.Text))
            {
                if (Validates.Contrasenia(txtPass.Password))
                {
                    UsuarioDAL dal = new UsuarioDAL();
                    
                    if (dal.Login(txtUser.Text.Trim(), txtPass.Password.Trim()).Rows.Count > 0)
                    {
                        winBienvenida win = new winBienvenida();
                        win.ShowDialog();
                    }
                    else
                    {
                        MessageBox.Show("Usuario no registrado");
                    }

                }
                else
                {
                    MessageBox.Show("Contraseña no valida");
                }
            }
            else
            {
                MessageBox.Show("Nombre de Usuario no valido, el nombre de usuario debe tener entre 8 y 12 caracteres");
            }
        }
    }
}
