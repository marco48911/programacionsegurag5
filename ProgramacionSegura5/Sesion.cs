﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProgramacionSegura5
{
    public static class Sesion
    {
        public static int idSesion=0;
        public static string usuarioSesion=string.Empty;
        public static string rolSesion;

        public static string VerInfo()
        {
            return "Usuario: " + usuarioSesion + ", Rol: " + rolSesion;
        }

    }
}
