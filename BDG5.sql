USE [master]
GO
/****** Object:  Database [ProgramacionSegura]    Script Date: 12/03/2020 18:57:05 ******/
CREATE DATABASE [ProgramacionSegura]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ProgramacionSegura', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\ProgramacionSegura.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ProgramacionSegura_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\ProgramacionSegura_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ProgramacionSegura].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ProgramacionSegura] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProgramacionSegura] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProgramacionSegura] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ProgramacionSegura] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProgramacionSegura] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ProgramacionSegura] SET  MULTI_USER 
GO
ALTER DATABASE [ProgramacionSegura] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProgramacionSegura] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProgramacionSegura] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProgramacionSegura] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ProgramacionSegura] SET DELAYED_DURABILITY = DISABLED 
GO
USE [ProgramacionSegura]
GO
/****** Object:  Table [dbo].[Rol]    Script Date: 12/03/2020 18:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[idRol] [int] NOT NULL,
	[tipo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 12/03/2020 18:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[idUsuario] [int] NOT NULL,
	[usuario] [varchar](50) NOT NULL,
	[password] [varbinary](50) NOT NULL,
	[nombre] [varchar](50) NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UsuarioRol]    Script Date: 12/03/2020 18:57:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioRol](
	[idRol] [int] NOT NULL,
	[idUsuario] [int] NOT NULL,
 CONSTRAINT [PK_UsuarioRol] PRIMARY KEY CLUSTERED 
(
	[idRol] ASC,
	[idUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Rol] ([idRol], [tipo]) VALUES (1, N'CREAR REGISTROS')
INSERT [dbo].[Rol] ([idRol], [tipo]) VALUES (2, N'MODIFICAR REGISTROS')
INSERT [dbo].[Rol] ([idRol], [tipo]) VALUES (3, N'VER REGISTROS')
INSERT [dbo].[Usuario] ([idUsuario], [usuario], [password], [nombre]) VALUES (1, N'administrador', 0x94E9CAD52C84C0E7971A83A7109C35A8, N'juanPerez')
INSERT [dbo].[UsuarioRol] ([idRol], [idUsuario]) VALUES (1, 1)
INSERT [dbo].[UsuarioRol] ([idRol], [idUsuario]) VALUES (2, 1)
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioRol_Rol] FOREIGN KEY([idRol])
REFERENCES [dbo].[Rol] ([idRol])
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_UsuarioRol_Rol]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD  CONSTRAINT [FK_UsuarioRol_Usuario] FOREIGN KEY([idUsuario])
REFERENCES [dbo].[Usuario] ([idUsuario])
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_UsuarioRol_Usuario]
GO
USE [master]
GO
ALTER DATABASE [ProgramacionSegura] SET  READ_WRITE 
GO
